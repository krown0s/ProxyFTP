#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/socket.h>
#include  <netdb.h>
#include  <string.h>
#include  <unistd.h>
#include  <stdbool.h>

#define SERVADDR "localhost"        // Définition de l'adresse IP d'écoute
#define SERVPORT "0"                // Définition du port d'écoute,
                                    // si 0 port choisi dynamiquement
#define LISTENLEN 1                 // Taille du tampon de demande de connexion
#define MAXBUFFERLEN 1024
#define MAXHOSTLEN 64
#define MAXPORTLEN 6

int main()
{
	int ecode;                       // Code retour des fonctions
	char serverName[MAXHOSTLEN];     // Nom de la machine serveur
	char serverAddr[MAXHOSTLEN];     // Adresse du serveur
	char serverPort[MAXPORTLEN];     // Port du server
	int descSockRDV;                 // Descripteur de socket de rendez-vous
	int descSockCOM;                 // Descripteur de socket de communication
	struct addrinfo hints;           // Contrôle la fonction getaddrinfo
	struct sockaddr_storage myinfo;  // Informations sur la connexion de RDV
	struct sockaddr_storage from;    // Informations sur le client connecté
	socklen_t len;                   // Variable utilisée pour stocker les
	  		                         // longueurs des structures de socket
	char buffer[MAXBUFFERLEN];       // Tampon de communication entre le client et le serveur

	int descSock;                  // Descripteur de la socket
	struct addrinfo *res,*resPtr;  // Résultat de la fonction getaddrinfo

	bool isConnected = false;      // booléen indiquant que l'on est bien connecté

	// Publication de la socket au niveau du système
	// Assignation d'une adresse IP et un numéro de port
	// Mise à zéro de hints
	memset(&hints, 0, sizeof(hints));

	// Initailisation de hints
	hints.ai_flags = AI_PASSIVE;      // mode serveur, nous allons utiliser la fonction bind
	hints.ai_socktype = SOCK_STREAM;  // TCP
	hints.ai_family = AF_INET;      // les adresses IPv4 et IPv6 seront présentées par
				                         // la fonction getaddrinfo

	// Récupération des informations du serveur
	ecode = getaddrinfo(SERVADDR, SERVPORT, &hints, &res);
	if (ecode)
	{
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(ecode));
		exit(1);
	}

	//Création de la socket IPv4/TCP
	descSockRDV = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (descSockRDV == -1)
	{
		perror("[ Erreur ] Creation socket");
		exit(4);
	}

	// Publication de la socket
	ecode = bind(descSockRDV, res->ai_addr, res->ai_addrlen);
	if (ecode == -1)
	{
		perror("[ Erreur ] Liaison de la socket de RDV");
		exit(3);
	}
	// Nous n'avons plus besoin de cette liste chainée addrinfo
	freeaddrinfo(res);
	// Récuppération du nom de la machine et du numéro de port pour affichage à l'écran

	len=sizeof(struct sockaddr_storage);
	ecode=getsockname(descSockRDV, (struct sockaddr *) &myinfo, &len);
	if (ecode == -1)
	{
		perror("SERVEUR: getsockname");
		exit(4);
	}
	ecode = getnameinfo((struct sockaddr*)&myinfo, sizeof(myinfo), serverAddr,MAXHOSTLEN,
                         serverPort, MAXPORTLEN, NI_NUMERICHOST | NI_NUMERICSERV);

	if (ecode != 0)
	{
		fprintf(stderr, "error in getnameinfo: %s\n", gai_strerror(ecode));
		exit(4);
	}

	printf("[ Info ] L'adresse d'ecoute est : %s\n", serverAddr);
	printf("[ Info ] Le port d'ecoute est : %s\n", serverPort);
	// Definition de la taille du tampon contenant les demandes de connexion
	ecode = listen(descSockRDV, LISTENLEN);
	if (ecode == -1)
	{
		perror("[ Erreur ] Initialisation buffer d'écoute");
		exit(5);
	}

	len = sizeof(struct sockaddr_storage);
	// Attente connexion du client
	// Lorsque demande de connexion, creation d'une socket de communication avec le client
	descSockCOM = accept(descSockRDV, (struct sockaddr *) &from, &len);
	if (descSockCOM == -1)
	{
		perror("[ Erreur ] Accept\n");
		exit(6);
	}
	// Echange de données avec le client connecté
	strcpy(buffer, "Veuillez vous identifier avec nomlogin@nomserveur\n");
	write(descSockCOM, buffer, strlen(buffer));

	// Attente reponse client
	//Echange de donneés avec le client
	ecode = read(descSockCOM, buffer, MAXBUFFERLEN);
	if (ecode == -1) {perror("[ Erreur ] Problème de lecture\n"); exit(3);}
	buffer[ecode] = '\0';
	printf("[ Client ] %s\n",buffer);

	char cmduser [MAXBUFFERLEN] = "USER ";
	char user [MAXBUFFERLEN];
  //Decoupe le message
  sscanf(buffer,"%[^@]@%[^\n]", user, serverName);
	strcat(cmduser, user);
	strcat(cmduser, "\n");

	serverName[strlen(serverName)]=0;
  printf("[ Info ] Utilisateur : %s\n", user);
  printf("[ Info ] Commande utilisateur : %s", cmduser);
  printf("[ Info ] Serveur FTP : %s\n",serverName);

	//Récupération des informations sur le serveur
	ecode = getaddrinfo(serverName,"21",&hints,&res);
	if (ecode)
	{
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(ecode));
		exit(1);
	}

	resPtr = res;

	while(!isConnected && resPtr!=NULL)
	{
		//Création de la socket IPv4/TCP
		descSock = socket(resPtr->ai_family, resPtr->ai_socktype, resPtr->ai_protocol);
		if (descSock == -1)
		{
			perror("[ Erreur ] Creation socket");
			exit(2);
		}

  	//Connexion au serveur
		ecode = connect(descSock, resPtr->ai_addr, resPtr->ai_addrlen);
		if (ecode == -1)
		{
			resPtr = resPtr->ai_next;
			close(descSock);
		}
		// On a pu se connecté
		else isConnected = true;
	}

	freeaddrinfo(res);

	if (!isConnected)
	{
		perror("[ Erreur ] Connexion impossible");
		exit(2);
	}

	//Echange message 220 avec le serveur connexion
	ecode = read(descSock, buffer, MAXBUFFERLEN);
	if (ecode == -1) {perror("[ Erreur ] Problème de lecture\n"); exit(3);}
	buffer[ecode] = '\0';
	printf("[ Serveur FTP ] %s",buffer);
	write(descSockCOM, buffer, strlen(buffer));
	// Echange user avec le serveur connecté

	//User
	//ecode = read(descSockCOM, buffer, MAXBUFFERLEN);
	//if (ecode == -1) {perror("[ Erreur ] Problème de lecture\n"); exit(3);}
	//buffer[ecode] = '\0';
	//printf("[ Client ] \"%s\".\n",buffer);
	//strcat(buffer,"\n");

	write(descSock, cmduser, strlen(cmduser));

	//Echange message 331 avec le serveur
	ecode = read(descSock, buffer, MAXBUFFERLEN);
	if (ecode == -1) {perror("[ Erreur ] Problème de lecture\n"); exit(3);}
	buffer[ecode] = '\0';
	printf("[ Serveur FTP ] %s",buffer);

	// Echange de données avec le client connecté
	write(descSockCOM, buffer, strlen(buffer));

	// Attente reponse client
	//Echange mdp avec le client
	ecode = read(descSockCOM, buffer, MAXBUFFERLEN);
	if (ecode == -1) {perror("Problème de lecture\n"); exit(3);}
	buffer[ecode] = '\0';
	printf("[ Client ] %s\n",buffer);

	// Echange mdp avec le serveur connecté
	char cmdmdp [MAXBUFFERLEN] = "PASS ";
	strcat(cmdmdp, buffer);
	strcat(cmdmdp, "\n");
	printf("[ Info] Commande mot de passe : %s", cmdmdp);
	write(descSock, cmdmdp, strlen(cmdmdp));

	ecode = read(descSock, buffer, MAXBUFFERLEN);
	if (ecode == -1) {perror("Problème de lecture\n"); exit(3);}
	buffer[ecode] = '\0';
	printf("[ Serveur FTP ] %s",buffer);
	strcat(buffer,"\n");
	write(descSockCOM, buffer, strlen(buffer));

	//Fermeture de la connexion
	close(descSockCOM);
	close(descSockRDV);
}
